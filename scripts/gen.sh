#!/usr/bin/env bash

##################################
##                              ##
##      © _.-= vint_243 =-._    ##
##                              ##
##################################

openvpn_dir="/etc/openvpn"
rsa="easy"
key="keys"
serv="server"
cl="client"

serv_conf="./done/$serv/server.conf"
cl_conf="./done/$cl/client.conf"

mk_prop(){

echo 
echo "Create dir from certs, keys and conf"
echo

mkdir -p ./done/$serv/$key
mkdir -p ./done/$serv/ccl/

}

init_cc(){

echo
echo "Initial Center Certificate"
echo

./$rsa/easyrsa init-pki

}

gen_ta(){

echo
echo "Generate TLS Authentification key"
echo

openvpn --genkey --secret ./pki/ta.key

}

gen_dh(){

echo
echo "Generation Devi-Helman Key"
echo

./$rsa/easyrsa gen-dh

}

build_ca(){

echo
echo "Build Center Certificate"
echo

./$rsa/easyrsa build-ca

}

create_serv_cert(){

echo
echo "Create Server Certificate"
echo

echo
read -p "Name server certificate :  "  srv_cert
echo

./$rsa/easyrsa build-server-full $srv_cert

echo
read -p "Enter password from you server certificate :  " serv_psk
echo

echo $serv_psk > ./done/$serv/$key/psk

}

create_cl_cert(){

echo
echo "Create Client Certificate"
echo

echo
read -p "Name client certificate :  " cl_cert
echo

./$rsa/easyrsa build-client-full $cl_cert

echo
read -p "Enter password from you client certificate :  " client_psk
echo

mkdir -p ./done/$cl/$cl_cert/$key/

echo $client_psk > ./done/$cl/$cl_cert/$key/psk 

}

####
###
# Keys

cp_srv_cert(){

echo
echo "Coping Server Certificate,Keys from Directory"
echo

#  Server

cp -v ./pki/ta.key ./done/$serv/$key/
cp -v ./pki/ca.crt ./done/$serv/$key/
cp -v pki/private/ca.key ./done/$serv/$key
cp -v pki/dh.pem ./done/$serv/$key
cp -v pki/issued/$srv_cert.crt ./done/$serv/$key
cp -v pki/private/$srv_cert.key ./done/$serv/$key

}

cp_cl_cert(){

# Client

echo
echo "Coping Client Certificate,Keys from Directory"
echo

cp -v ./pki/ta.key ./done/$cl/$cl_cert/$key
cp -v ./pki/ca.crt ./done/$cl/$cl_cert/$key/
cp -v pki/private/ca.key ./done/$cl/$cl_cert/$key
cp -v pki/dh.pem ./done/$cl/$cl_cert/$key
cp -v pki/issued/$cl_cert.crt ./done/$cl/$cl_cert/$key
cp -v pki/private/$cl_cert.key ./done/$cl/$cl_cert/$key

}

serv_conf(){

echo
read -p "Enter ip address from server from WAN interface :  " serv_ip
echo

echo
read -p  "Enter port vpn server from WAN interface :  " serv_port
echo

echo
echo "Server IP   = $serv_ip"
echo "Server PORT = $serv_port"
echo

}

####
###
#  Conf

##
#  Server

serv_conf_fast(){

cat << EOF > $serv_conf
mode server
proto tcp-server
dev tun
port $serv_port # Порт
keepalive 30 500
#daemon
server 130.0.1.0 255.255.255.0
#push "route 10.0.2.0 255.255.255.0"
ifconfig-pool-persist ipp.txt
auth-nocache
#ifconfig 10.10.0.1 255.255.255.0 # Внутренний IP сервера
#ifconfig-pool 10.10.0.2 10.10.0.128 # Пул адресов.
EOF

echo
read -p "Redirect all trafic from VPN Server (y/n) :  " redir_get
echo

if [[ $redir_get == "y" ]] ;then 
	
	echo 'push "route 0.0.0.0 0.0.0.0"' >> $serv_conf
	echo 'push "dhcp-option DNS 1.1.1.1"' >> $serv_conf
	echo 'push "dhcp-option DNS 1.0.0.1"' >> $serv_conf

      else 

      	echo '#push "redirect-gateway def1" ' >> $serv_conf

fi

cat << EOF >> $serv_conf
#push "route-gateway 10.10.0.1"
client-to-client
client-config-dir /etc/openvpn/ccl
tls-server
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384
cipher AES-256-CBC
dh /etc/openvpn/keys/dh.pem
ca /etc/openvpn/keys/ca.crt
tls-auth /etc/openvpn/keys/ta.key 0
cert /etc/openvpn/keys/$srv_cert.crt
key /etc/openvpn/keys/$srv_cert.key
askpass /etc/openvpn/keys/psk
duplicate-cn
persist-key
persist-tun
comp-lzo
log /var/log/openvpn/openvpn.log
status /var/log/openvpn/openvpn-status.log 1
status-version 3
verb 3

EOF

echo "$serv_ip" > $serv_conf.ip

}

serv_conf_adv(){

echo
read -p "Enter ip and netmask from VPN network (example 130.0.1.0 255.255.255.0) :  " vpn_net_ip
echo

echo
read -p "Enter ip and netmask from VPN tunnel (example 10.0.2.0 255.255.255.0) :  " vpn_tun_ip
echo

echo
read -p  "Enter protocol from work VPN server (tcp or udp) :   " vpn_protocol
echo

cat << EOF > $serv_conf
mode server
proto $vpn_protocol
dev tun
port $serv_port # Порт
keepalive 30 500
#daemon
server $vpn_net_ip 
#push "route $vpn_tun_ip "
ifconfig-pool-persist ipp.txt
auth-nocache
#ifconfig 10.10.0.1 255.255.255.0 # Внутренний IP сервера
#ifconfig-pool 10.10.0.2 10.10.0.128 # Пул адресов.
EOF

echo
read -p "Redirect all trafic from VPN Server (y/n) :  " redir_get
echo

if [[ $redir_get == "y" ]] ;then

        echo 'push "route 0.0.0.0 0.0.0.0"' >> $serv_conf
        echo 'push "dhcp-option DNS 1.1.1.1"' >> $serv_conf
        echo 'push "dhcp-option DNS 1.0.0.1"' >> $serv_conf

      else

        echo '#push "redirect-gateway def1"' >> $serv_conf

fi

echo
read -p "Accept connect client-client from VPN Server (y/n) :  " cl_cl
echo

if [[ $cl_cl == "y" ]] ;then

        echo 'client-to-client' >> $serv_conf

      else

        echo '#client-to-client' >> $serv_conf

fi

cat << EOF >> $serv_conf
client-config-dir /etc/openvpn/ccl
tls-server
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384
cipher AES-256-CBC
dh /etc/openvpn/keys/dh.pem
ca /etc/openvpn/keys/ca.crt
tls-auth /etc/openvpn/keys/ta.key 0
cert /etc/openvpn/keys/$srv_cert.crt
key /etc/openvpn/keys/$srv_cert.key
askpass /etc/openvpn/keys/psk
EOF

echo
read -p "Accept connect client on one certificate from VPN Server (y/n) :  " dubl_cl
echo

if [[ $dubl_cl == "y" ]] ;then

        echo 'duplicate-cn' >> $serv_conf

      else

        echo '#duplicate-cn' >> $serv_conf

fi

cat << EOF >> $serv_conf
persist-key
persist-tun
comp-lzo
log /var/log/openvpn/openvpn.log
status /var/log/openvpn/openvpn-status.log 1
status-version 3
verb 3

EOF

echo "$serv_ip" > $serv_conf.ip

}

cl_conf(){

##
#  Client

cl_prot=`grep "proto" $serv_conf | awk ' {print $2} ' | cut -d "-" -f 1`
cl_port=`grep "port" $serv_conf | awk ' {print $2} '`
cl_route=`grep "server" $serv_conf | sed '/mode\|proto\|tls/d' | awk ' {print $2,$3} '`
cl_redirect=`grep "push" $serv_conf `
serv_ip=`cat $serv_conf."ip"`
remote=`echo $serv_ip`

cat << EOF > $cl_conf
proto $cl_prot-client
dev tun
remote $remote
port $cl_port
pull
resolv-retry infinite
#route $cl_route 
#$cl_redirect
keepalive 30 500
auth-nocache
tls-client
tls-cipher TLS-DHE-RSA-WITH-AES-256-GCM-SHA384
cipher AES-256-CBC
tls-auth /etc/openvpn/keys/ta.key 1
ca /etc/openvpn/keys/ca.crt
dh /etc/openvpn/keys/dh.pem
cert /etc/openvpn/keys/$cl_cert.crt
key /etc/openvpn/keys/$cl_cert.key
askpass /etc/openvpn/keys/psk
persist-key
persist-tun
comp-lzo
log-append  /var/log/openvpn/openvpn-client.log
verb 3
status /var/log/openvpn/openvpn-status.log 1
status-version 3

EOF

echo
read -p "Using personal ip address client from VPN network (y/n) :  " cl_ip_set
echo

if [[ $cl_ip_set == "y" ]] ; then
	
	echo
	read -p "Enter ip address from $cl_route network : " cl_ip
	echo
	
	echo
	echo "Client ip = $cl_ip"
	echo

	cl_mask=`echo $cl_route | awk ' {print $2} '`

	echo "ifconfig-push $cl_ip $cl_mask" > 	./done/$serv/ccl/$cl_cert

     else
      
        echo
	echo "IP address client using auto from VPN network"
	echo

fi

}

list_iface (){

dir_cl_net="/sys/class/net"
ls_iface=`cat /proc/net/dev  | awk '{print $1}' | grep ":" | cut -d ":" -f 1`

for iface in $ls_iface;  do
        
	echo
        if_state=`cat $dir_cl_net/$iface/operstate`
        echo $iface  $if_state

done

}

firewall_set_filter(){

iptables_conf="/etc/sysconfig/iptables"

echo
echo "Add iptables rules from ACCEPT connect to $cl_port $cl_prot port "
echo

iptables -t filter -A INPUT -p $cl_prot -m state --state NEW -m $cl_prot --dport $cl_port -j ACCEPT
iptables -t filter -D INPUT -j REJECT --reject-with icmp-host-prohibited
iptables -t filter -A INPUT -j REJECT --reject-with icmp-host-prohibited

iptables-save > $iptables_conf
#sed '/COMMIT/d' $iptables_conf > $iptables_conf 
#echo "-A INPUT -p tcp -m state --state NEW -m $cl_prot --dport $cl_port ACCEPT" >> $iptables_conf
#echo 'COMMIT' >> $iptables_conf

}

firewall_set_nat(){

echo
echo "Add NAT rules from iptables"
echo

echo
echo "List network interface"
echo

list_iface

cl_route_net=`grep "server" $serv_conf | sed '/mode\|proto\|tls/d' | awk ' {print $2"/"$3} '`

echo
read -p "Enter WAN interface :  " wan
echo

iptables -t nat -A POSTROUTING -o tun0 -j MASQUERADE
iptables -t nat -A POSTROUTING -s $cl_route_net -o $wan -j MASQUERADE

#sysctl net.ipv4.ip_forward=1

sed '/ip_forward/d' /etc/sysctl.conf > /etc/sysctl.conf && echo "net.ipv4.ip_forward = 1" >> /etc/sysctl.conf
sysctl -p/etc/sysctl.conf

}

os_detect(){

gentoo_os=`cat /etc/os-release | grep NAME | sed '/PRETTY\|CPE/d' | awk ' {print $1} ' | cut -f 2 -d "="`
binary_os=`cat /etc/os-release | grep NAME | sed '/PRETTY\|CPE/d' | awk ' {print $1} ' | cut -f 2 -d "=" | cut -f 2 -d '"'`

}

unit_find(){

systemd='/usr/lib/systemd'
openvpn_unit='/usr/lib/systemd/system/openvpn@server.service'

unit_copy="./systemd"

if [[ -d $systemd ]] ; then
        
        if [[ -f $openvpn_unit ]] ; then
        
                echo
                echo "Systemd unit found"
                echo
             
             else
                
                echo
                echo "Systemd unit not found"
                echo

                cp -vR $unit_copy/* $systemd/system/

        fi

fi

}

pack_files_serv(){

echo
echo "Copying server certificates, keys and config files "
echo

cp -vR ./done/$serv/* /etc/openvpn/

mkdir /var/log/openvpn/

unit_find

}

pack_files_cl(){

echo
echo "Packing client certificates, keys and config files "
echo

mv $cl_conf ./done/$cl/$cl_cert/
mkdir -p ./done/$cl/$cl_cert/systemd/
cp -vR $unit_copy/* ./done/$cl/$cl_cert/systemd/
cd ./done/$cl/$cl_cert/ && tar -cvjpf ../../$cl_cert.tar.bz2 ./

echo 
echo "Push remote host client done/$cl_cert.tar.bz2"
echo

cd ../../../
}

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,------------------------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\tGeneration VPN certificate, keys and config file\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '------------------------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Fast generation"
echo -e "\t2. Custom generation"
echo -e "\t3. Generation dop client certificate"
echo -e "\t4. Add firewall rules"
echo
echo -e "\t0. Exit"
echo 
echo -en "\t\t" ; read -p "Enter number operation : " -n 1 main_opt
echo
echo

case $main_opt in

0)
        break ;;
1)
        mk_prop
	init_cc
	gen_ta
	gen_dh
	build_ca
	create_serv_cert
	create_cl_cert
	cp_srv_cert
	cp_cl_cert
	serv_conf
	serv_conf_fast
	cl_conf
	#firewall_set_filter
	pack_files_serv
	pack_files_cl;;
2)
        mk_prop
	init_cc
	gen_ta	
	gen_dh
	build_ca
	create_serv_cert
	create_cl_cert
	cp_srv_cert
	cp_cl_cert
	serv_conf
	serv_conf_adv
	cl_conf
	#firewall_set_filter
	pack_files_serv
	pack_files_cl;;
3)
        create_cl_cert
	cp_cl_cert
	cl_conf
	pack_files_serv
	pack_files_cl;;
4)	
	firewall_set_nat;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac

echo
echo
echo -en "\n\n\t\t\t" ; read -p "Please any key " -n 1 line
echo

done

clear


